##Snout
##Mp3 to WAV Python Converter
##by Donnie Christianson
##Copyright 2003/2004 Donnie Christianson

##6/12/04
##Build Alpha .3
##Menu Build Alpha .1

##Initial script written on 11/22/03
##Mp3 to Wav Simple Open/Read/Write functions

##Updated functions 5/14/04: (Build Alpha .2)
##==========================
## Added support for displaying the waveform
##==========================

##Updated 06/12/04: (Build Alpha .3)
##==========================
## Added Simple User Menu Module (menu is at version Alpha .1)
##==========================

##Updated 06/14/04: (Build Alpha .4)
##==========================
## Completed conversion function to prompt user input for filename
## (Conversion process dependent on user typing .wav extension in filename)
## Menu at Alpha .2
##==========================


##IMPORTANT!!!
##This program requires tKinter and the Snack Sound Toolkit for Python
##These are free extensions to python (links will be provided here soon)


##=========
##=========

##To Do:


##--optimize the conversion function to include a yes/cancel feature

##--add an Mp3 directory for storing the Mp3 files to be converted

##--add a WAV directory for saving the converted WAV files to

##--add directory support in open and convert functions for above directories
##--add support for listing the contents of the above directories

##--create a sound player module that the user can control to play the sounds

##--add user-defined directory support (e.g., user is prompted to enter the directory
##where mp3 files are stored on their hard drive, other media, or desktop

##--add support for listing the contents of the user-defined directory

##--add support for .ogg file type
##--add support for .aiff file type

##--change the conversion function to a convert.py file that is a callable module
##(for portability and use by future Snout add-on modules)

##--garbage cleanup - clear the audio files in memory in the app after they are converted

##--restrict the GUI around the waveform display (currently is resizable, but the waveform doesn't resize)

##--garbage cleanup for GUI/tKinter objects (currently do not close when user closes the script window)

##=========
##=========


##housekeeping section

import os
import sys
import os.path

import wave

import Tkinter 

##-invoking the required Snack sound toolkit

from Tkinter import *
root = Tk()
import tkSnack
tkSnack.initializeSnack(root)


##Need to implement or import the user menu here.
##Update: 6/12/04 - DONE. This is an alpha version:

#Menu for Snout Mp3 to WAV Converter (Alpha .1)
##by Donnie Christianson
##June 12th, 2004
##Copyright 2004 Donnie Christianson


##Declare mysound as a GLOBAL variable to hold opened file
mysound = tkSnack.Sound()


def DisplayMenu():
    print '  MENU DEMO PROGRAM'
    print
    print '     1.  Open A File'
    print
    print '     2.  Display File Waveform'
    print
    print '     3.  Convert File'
    print
    print '     4.  Exit Program'
    print

def Option1Function():##This is the Open Command
    print '-------------'
    print 'Opening file...'
    print '-------------'
    
    ##--opening the file to be converted
    ##--may need to add a fileexists comparison or statement to determine the file exists in directory

    myfile = raw_input('type filename: ')

    open(myfile)
    print "Opening file, please wait..."

    #designating myfile as a Snack sound object
    global mysound
    mysound.read(myfile)
    info = mysound.info(myfile)
    print 'File information:', myfile, info
    print 'File is Open!'


    
def Option2Function(): #This is the command to display the waveform of the opened file
    print '-------------'
    print 'Displaying Waveform, please wait...'
    ##--display the waveform of imported file in a tKinter/Snack Canvas object
    c = tkSnack.SnackCanvas(root, height=100)
    c.pack()
    c.create_waveform(0, 0, sound=mysound, height=100, zerolevel=1)

    print "Waveform Display Complete!"
    print '-------------'
    
def Option3Function():

    ##---converts the file to WAV format provided the user
    ##---specifies the .wav extension in the new filename

    global mysound

    myfilename = raw_input('Enter the new filename: ')

    ##perform the conversion process here - save the file, then
    ##give the user a confirmation that it was done
    
    print '-------------'
    print "Saving file, please wait..."
    
    mysound.write(myfilename)
    
    print 'File Saved!"
    print '-------------'

#------------------
    ##for when multiple formats are supported in future versions:
    ##mynewformat = raw_input('Specify the format: ')
    ##print "Converting file to ", mynewformat, ", please wait..."

#------------------


#-----------------------------------------

    ##---old, automatic, code that worked:
    ##mynewfile = mysound
    ##mynewfile.write('mynewfile.wav')
    ##print "File Converted!"


#Main program

#Housekeeping
DisplayMenu()
choice = int(raw_input('  Enter your choice:  '))

#Main Loop
while choice != 4:
    if choice == 1:
       Option1Function()
    elif choice == 2:
        Option2Function()
    elif choice ==3:
        Option3Function()
    DisplayMenu()
    choice = int(raw_input('  Enter your choice:  '))

#Finish up
print 'Thanks for using Snout!'


##mysound.play() ---this function will be added later and
##wrapped with a built-in sound player to display the progress in real-time
##with a waveform display and timeline

##look at adding effects or processing after the audio basics are covered
##(by that I mean ability to reverse the file/waveform, echo, etc.)


