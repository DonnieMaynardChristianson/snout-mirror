##Snout
##Mp3 to WAV Python Converter
##by Donnie Christianson
##Copyright 2003/2004 Donnie Christianson

##6/12/04
##Build Alpha .3
##Menu Build Alpha .1

##Initial script written on 11/22/03
##Mp3 to Wav Simple Open/Read/Write functions

##Updated functions 5/14/04: (Build Alpha .2)
##==========================
## Added support for displaying the waveform
##==========================

##Updated 06/12/04: (Build Alpha .3)
##==========================
## Added Simple User Menu Module (menu is at version Alpha .1)
##==========================

##Updated 06/14/04: (Build Alpha .4)
##==========================
## Completed conversion function to prompt user input for filename
## (Conversion process dependent on user typing .wav extension in filename)
## Menu at Alpha .2
##==========================


##IMPORTANT!!!
##This program requires tKinter and the Snack Sound Toolkit for Python
##These are free extensions to python (links will be provided here soon)


##=========
##=========

##To Do:

##--optimize the conversion function to include a yes/cancel feature

##--add an Mp3 directory for storing the Mp3 files to be converted

##--add a WAV directory for saving the converted WAV files to

##--add directory support in open and convert functions for above directories
##--add support for listing the contents of the above directories

##--create a sound player module that the user can control to play the sounds

##--add user-defined directory support (e.g., user is prompted to enter the directory
##where mp3 files are stored on their hard drive, other media, or desktop

##--add support for listing the contents of the user-defined directory

##--add support for .ogg file type
##--add support for .aiff file type

##--change the conversion function to a convert.py file that is a callable module
##(for portability and use by future Snout add-on modules)

##--garbage cleanup - clear the audio files in memory in the app after they are converted

##--restrict the GUI around the waveform display (currently is resizable, but the waveform doesn't resize)

##--garbage cleanup for GUI/tKinter objects (currently do not close when user closes the script window)


##=========
##=========




----------------------------------------------------

About Snout

Snout is an Mp3 to WAV Converter for Python 2.3.4.

This program is very easy to use, and extremely fast. Currently it is in "script form" only, but a 
GUI interface version is being created that will run stand-alone in Windows. However, this version will 
still be updated and development will be continued alongside the stand-alone version. This "pure" version 
is handy for when you need a fast, cross-platform conversion script.

Snout works on Windows, Macintosh, and Linux machines running Python 2.3.4 with the required extensions. 
Snack and tKinter toolkits are required to use Snout.

Please read the Snout Manual.txt file for instructions on loading and converting your Mp3's. Yes, I 
probably could have just put this stuff here in the ReadMe, but I wanted an "official" manual for Snout. 
And, as the program grows, it will need the room (!)

Thanks for downloading Snout. This is my first "real" program, written over the last several months. 
Of course the actual development time was probably only a few weeks at most, it took this long due to 
other commitments and projects. I hope you enjoy using Snout!




----------------------------------------------------

License/Restrictions/Usage Info:

Snout is Copyright 2004 by Donnie Christianson.

You may not sell the source code, the program, or any of its components.

If you want to improve on Snout or add modules and/or functionality to Snout, please contact Donnie at the 
email address below:

donnie@posertrax.com.

You may share the program in its entirety, provided the scripts and information files are intact and 
unaltered in any way. 

